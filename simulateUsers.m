%% simulateUsers
% 
% Simulates users interacting with a set of runs, according to the provided
% Markovian model.

%% Synopsis
%
%   [P_H, G, H] = simulateUsers(model, U, runs, relevanceLoss)
%  
% It assumes binary relevance, otherwise it will not work properly.
%
% *Parameters*
%
% * *|model|* - the Markovian model to adopt. It is a struct where each of
% the fields is one of the p/q/s parameters of the transition matrix.
% * *|U|* - the number of users to simulate.
% * *|runs|* - the runs to evaluate. A matrix of |0| (not relevant) and |1|
% relevant; each row is a different run; the left-most column is the
% top-ranked document, the right-most column is the bottom-ranked document.
% * *|relevanceLoss|* - the relevance loss for visiting a document more
% than once.
%
%
% *Returns*
%
% * |P_H|  - our P@H measure.
% * |G|  - the numerator of the P@H measure.
% * |H|  - the denominators of the P@H measure.
%
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2020 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>
%
%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [P_H, G, H] = simulateUsers(model, U, runs, relevanceLoss)

    narginchk(4, 4);
        
    % check that model is a struct with the expected fields
    assert(isstruct(model), ...
            'MATTERS:IllegalArgument', 'Expected model to be a struct.');
    
    assert(all(isfield(model, {'p', 'p1', 'pN', 'q', 'q1', 'qN', 's', 's1', 'sN'})), ...
            'MATTERS:IllegalArgument', 'Expected model to contain the following fields: p, p1, pN, q, q1, qN, s, s1, sN.');
        
    % check that model (p1, q1, s1) sum to 1
    assert(model.p1 + model.q1 + model.s1 == 1.0 , ...
            'MATTERS:IllegalArgument', 'Expected model.p1 + model.q1 + model.s1 to be equal to 1.0.');
        
    % check that model (p, q, s) sum to 1
    assert(model.p + model.q + model.s == 1.0 , ...
            'MATTERS:IllegalArgument', 'Expected model.p + model.q + model.s to be equal to 1.0.');
        
    % check that model (pN, qN, sN) sum to 1
    assert(model.pN + model.qN + model.sN == 1.0 , ...
            'MATTERS:IllegalArgument', 'Expected model.pN + model.qN + model.sN to be equal to 1.0.');        
                
    % check that U is a scalar integer greater than or equal to 1
    validateattributes(U, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1}, '', 'U');
        
    % check that runs is a integer matrix, whose values are greater than or 
    % equal to 0
    validateattributes(runs, {'numeric'}, ...
            {'nonempty', 'integer', '2d', '>=', 0}, '', 'runs');
                
    % check that relevanceLoss is a scalar integer greater than or equal to
    % 0 and less than or equal to 1 
    validateattributes(relevanceLoss, {'numeric'}, ...
            {'nonempty', 'scalar', '>=', 0, '<=', 1}, '', 'relevanceLoss');    
        
    % the total number of runs
    R = size(runs, 1);
    
    % the length of a run
    N = size(runs, 2);

    % the End state is just beyond the length of the run
    E = N + 1;

    % the initial number of steps to simulate
    S = N * 5;

    % the matrix representing the users (rows) and the steps of each user (columns)
    users = rand(U, S);

    % the current position in the ranking for all the users;
    % all the users start from position 1 in the ranking
    X = ones(U, 1);

    % the total number of steps performed by each user
    H = zeros(U, R);

    % the total gain accumulated by each user for each run
    G = zeros(U, R);
    
    % the relevance loss for each user at each rank position
    L = ones(U, N);

    % the column currently used in the user matrix
    j = 1;


    % continue until all the users reach the End state
    while any(X ~= E)

        % get the current step for all the users
        u = users(:, j);

        for i = 1:U

            % check in which ranking position is each user and decide how to
            % update the state
            switch X(i)

                % the initial position in the ranking
                case 1

                    % update the total number of steps and the total gain
                    H(i, :) = H(i, :) + 1;
                    G(i, :) = G(i, :) + runs(:, X(i)).' .* L(i, X(i));
                    
                    % reduce the relevance for the next steps
                    L(i, X(i)) = L(i, X(i)).*(1 - relevanceLoss);

                    % decide the transition to the next state
                    if u(i) <= model.p1
                        X(i) = X(i) + 1;
                    else
                        X(i) = E;
                    end

                % the last position in the ranking
                case N

                    % update the total number of steps and the total gain
                    H(i, :) = H(i, :) + 1;
                    G(i, :) = G(i, :) + runs(:, X(i)).' .* L(i, X(i));
                    
                    % reduce the relevance for the next steps
                    L(i, X(i)) = L(i, X(i)).*(1 - relevanceLoss);

                    % decide the transition to the next state
                    if u(i) <= model.qN
                        X(i) = X(i) - 1;
                    else
                        X(i) = E;
                    end

                % the user already reached the End state
                case E

                    % nothing  to  do for this user

                % any intermediate position in the ranking
                otherwise

                    % update the total number of steps and the total gain
                    H(i, :) = H(i, :) + 1;
                    G(i, :) = G(i, :) + runs(:, X(i)).' .* L(i, X(i));
                    
                    % reduce the relevance for the next steps
                    L(i, X(i)) = L(i, X(i)).*(1 - relevanceLoss);

                    % decide the transition to the next state
                    if u(i) <= model.p
                        X(i) = X(i) + 1;
                    elseif u(i) > model.p && u(i) <= model.p + model.q
                        X(i) = X(i) - 1;
                    else
                        X(i) = E;
                    end
            end


        end % for users

        % if we already used all the S steps, simulate another S
        if (j == S)
            %fprintf('- Generating additional %d steps\n', S);
            j = 1;
            users = rand(U, S);
        else
            j = j + 1;
        end

    end % for steps

    % the P@H measure for each user and for each run
    P_H = G./H;

end

