Matlab source code for running the experiments reported in the paper:

* Ferrante, M. and Ferro, N. (2020). Exploiting Stopping Time to Evaluate Accumulated Relevance. _Proc. 6th ACM SIGIR International Conference on the Theory of Information Retrieval (ICTIR 2020)_.

Use ph.m for generating Figures 1(a), 1(c), and 1(d) of the paper and ph_ap.m for Figure 1(b).