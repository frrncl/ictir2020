%% simulateAP
% 
% Simulates users interacting with a set of runs, according to the specific
% user model of Average Precision (AP).
%
% It is a version of *|simulateUsers|* specifically tailored on AP.

%% Synopsis
%
%   [m] = fast_ap(runs)
%  
% It assumes binary relevance, otherwise it will not work properly.
%
% *Parameters*
%
% * *|U|* - the number of users to simulate.
% * *|runs|* - the runs to evaluate. A matrix of |0| (not relevant) and |1|
% relevant; each row is a different run; the left-most column is the
% top-ranked document, the right-most column is the bottom-ranked document.
%
%
% *Returns*
%
% * |P_H|  - our P@H measure.
% * |G|  - the numerator of the P@H measure.
% * |H|  - the denominators of the P@H measure.
%
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2020 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>
%
%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [P_H, G, H] = simulateAP(U, runs)

    narginchk(2, 2);
                        
    % check that U is a scalar integer greater than or equal to 1
    validateattributes(U, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1}, '', 'U');
        
    % check that runs is a integer matrix, whose values are greater than or 
    % equal to 0
    validateattributes(runs, {'numeric'}, ...
            {'nonempty', 'integer', '2d', '>=', 0}, '', 'runs');
                        
    % the total number of runs
    R = size(runs, 1);
    
    % the length of a run
    N = size(runs, 2);

    % the End state is just beyond the length of the run
    E = N + 1;

    % the initial number of steps to simulate
    S = N * 5;

    % the matrix representing the users (rows) and the steps of each user (columns)
    users = rand(U, S);

    % the current position in each ranking for all the users;
    % all the users start from position 1 in the ranking
    X = ones(U, R);

    % the total number of steps performed by each user
    H = zeros(U, R);

    % the total gain accumulated by each user for each run
    G = zeros(U, R);
    
    % the column currently used in the user matrix
    j = 1;

    % the number of relevant retrieved documents up to each rank position
    RR = cumsum(runs, 2);
    RN = repmat(RR(:,end),  1, N);
    P =  (RN - RR)./ (RN - [zeros(size(RR, 1), 1) RR(:, 1:end-1)]);
    

    % continue until all the users reach the End state
    while any(X ~= E)

        % get the current step for all the users
        u = users(:, j);

        for i = 1:U
            
            for r =  1:R
                % check in which ranking position is each user and decide how to
                % update the state
                switch X(i, r)
                    
                    % the user is in the last rank  position
                    case N
                        
                        % update the total number of steps and the total gain
                        H(i, r) = H(i, r) + 1;
                        G(i, r) = G(i, r) + runs(r, X(i,  r));
                        
                        % go to the End state
                        X(i, r) = E;
                        
                    % the user already reached the End state
                    case E
                        
                        % nothing  to  do for this user
                        
                    % any other position in the ranking
                    otherwise
                        
                        % update the total number of steps and the total gain
                        H(i, r) = H(i, r) + 1;
                        G(i, r) = G(i, r) + runs(r, X(i,  r));
                        
                        % decide the transition to the next state
                        if u(i) <= P(r, X(i,  r))
                            X(i, r) = X(i, r) + 1;
                        else
                            X(i, r) = E;
                        end
                end
                
            end % for runs
            
        end % for users

        % if we already used all the S steps, simulate another S
        if (j == S)
            %fprintf('- Generating additional %d steps\n', S);
            j = 1;
            users = rand(U, S);
        else
            j = j + 1;
        end

    end % for steps

    % the P@H measure for each user and for each run
    P_H = G./H;

end

