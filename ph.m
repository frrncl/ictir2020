%% ph
% 
% Computes P@H with different browsing models.
%
% It generates Figure 1(a), 1(c), and 1(d) of the paper.
%
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2020 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>
%
%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%


clear all
close all


% Deterministic Forward Browsing Model (DFBM) . 
% It corresponds to Precision and generates Figure 1(a) of the paper
d1.p1 = 1.00;
d1.q1 = 0.00;
d1.s1 = 1 - d1.p1 - d1.q1;

d1.p = 1.00;
d1.q = 0.00;
d1.s = 1 - d1.p - d1.q;

d1.pN = 0.00;
d1.qN = 0.00;
d1.sN = 1 - d1.pN - d1.qN;

% Stochastic Forward Browsing Model (SFBM).
% It corresponds to RBP with p = 0.5 and generates Figure 1(c) of the
% paper.
fw1.p1 = 0.50;
fw1.q1 = 0.00;
fw1.s1 = 1 - fw1.p1 - fw1.q1;

fw1.p = 0.50;
fw1.q = 0.00;
fw1.s = 1 - fw1.p - fw1.q;

fw1.pN = 0.00;
fw1.qN = 0.00;
fw1.sN = 1 - fw1.pN - fw1.qN;

% Random Walk Browsing Model (RWBM) with equal probability for 
% relevant/not relevant,
% It generates Figure 1(d) of the paper
rw1.p1 = 0.75;
rw1.q1 = 0.00;
rw1.s1 = 1 - rw1.p1 - rw1.q1;

rw1.p = 0.50;
rw1.q = 0.25;
rw1.s = 1 - rw1.p - rw1.q;

rw1.pN = 0.00;
rw1.qN = 0.25;
rw1.sN = 1 - rw1.pN - rw1.qN;

% the number of users to simulate
U = 100000;

% the recall base
RB = 4;

% the runs
runs = [1 0 0 1 0 0 1 0 0 1; ...
        0 1 1 1 1 0 0 0 0 0];

% average precision
ap = fast_ap(runs)./RB;

% precision at 10
p10 = sum(runs, 2)./size(runs, 2);

% rank-biased precision
rbp = fast_rbp(runs, 0.5);
    
    

% the model to use 
model = rw1;
    
% simulate all the runs wrt same users
[P_H, G, H] = simulateUsers(model, U, runs, 0.25);


currentFigure = figure;
    
    %------ run 1
    [f, x] = ecdf(P_H(:, 1));
    xm(1) = mean(P_H(:, 1));    
    xm2(1) = mean(G(:, 1))./mean(H(:, 1));
    
    h(1) = stairs(x, f, '-b', 'LineWidth', 1.5);
    
    hold on
    plot([xm(1) xm(1)], [0 1], '--b', 'LineWidth', 1.5)
    plot([xm2(1) xm2(1)], [0 1], ':b', 'LineWidth', 1.5)
    
    %------ run 2
    [f, x] = ecdf(P_H(:, 2));
    xm(2) = mean(P_H(:, 2));
    xm2(2) = mean(G(:, 2))./mean(H(:, 2));
    
    h(2) = stairs(x, f, '-r', 'LineWidth', 1.5);
        
    plot([xm(2) xm(2)], [0 1], '--r', 'LineWidth', 1.5)
    plot([xm2(2) xm2(2)], [0 1], ':r', 'LineWidth', 1.5)
    
    
    ax = gca;
    ax.TickLabelInterpreter = 'latex';
    ax.FontSize = 24;
    
    ax.XLabel.Interpreter = 'latex';
    ax.XLabel.String = 'x';
    ax.XLim = [0 1];

    ax.YLabel.Interpreter = 'latex';
    ax.YLabel.String = 'F(x)';
    ax.XLim = [0 1];

   % title(sprintf('Simulated users = %d; Model: p_1 = %3.2f; q_1 = %3.2f; s_1 = %3.2f; p_i = %3.2f; q_i = %3.2f; s_i = %3.2f; p_N = %3.2f; q_N = %3.2f; s_N = %3.2f;', ...
   %     U, model.p1, model.q1, model.s1, model.p, model.q, model.s, model.pN, model.qN, model.sN))
    
    
    legend(h, sprintf('r (AP = %3.2f; P@10 = %3.2f; RBP = %3.2f)  ', ap(1), p10(1), rbp(1)), ...
        sprintf('s (AP = %3.2f; P@10 = %3.2f; RBP = %3.2f)  ', ap(2), p10(2), rbp(2)), ...
        'Location', 'NorthWest')
    
    
    currentFigure.PaperPositionMode = 'auto';
    currentFigure.PaperUnits = 'centimeters';
    currentFigure.PaperSize = [47 22];
    currentFigure.PaperPosition = [1 1 45 20];
    
    print(currentFigure, '-dpdf', 'ph.pdf');
    
